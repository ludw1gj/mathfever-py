# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-29 06:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django_mysql.models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Calculation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, unique=True)),
                ('slug', models.SlugField(editable=False, max_length=100, unique=True)),
                ('input_labels', django_mysql.models.ListTextField(models.CharField(max_length=500), size=None)),
                ('input_ids', django_mysql.models.ListTextField(models.CharField(max_length=500), size=None)),
                ('input_type', models.CharField(
                    choices=[('binary_str', 'Binary String'), ('integer_str', 'Integer String'),
                             ('decimal_Decimal', 'Decimal decimal.Decimal'), ('decimal_int', 'Decimal Integer'),
                             ('hexadecimal_str', 'Hexadecimal String')], max_length=100)),
                ('function', models.CharField(max_length=100)),
                ('meta', models.TextField(blank=True)),
            ],
            options={
                'ordering': ['title'],
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, unique=True)),
                ('slug', models.SlugField(editable=False, max_length=100, unique=True)),
                ('image', models.ImageField(upload_to='images/category-cards')),
                ('body', models.TextField(blank=True)),
                ('meta', models.TextField(blank=True)),
            ],
            options={
                'ordering': ['title'],
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('body', models.TextField()),
                ('image', models.ImageField(upload_to='images/posts')),
                ('status', models.CharField(choices=[('d', 'Draft'), ('p', 'Published')], max_length=1)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
            ],
        ),
        migrations.AddField(
            model_name='calculation',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calculations',
                                    to='calculation.Category'),
        ),
    ]
