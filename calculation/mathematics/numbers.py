from math import sqrt


def is_prime(n):
    """
    Checks if n is a prime number and returns if is/not a prime and why.

    :param n: Number to be checked if is a prime number or not
    :type n: int
    :return: a HTML sentence explaining why param n is/not a prime number
    :rtype: str
    """
    answer = ''
    if n <= 0:
        answer = '<p>{n} is not a prime number, because prime numbers are defined for integers greater than 1.</p>'.format(
            n=n)
    elif n == 1:
        answer = '<p>1 is not considered to be a prime number.</p>'
    elif n == 2:
        answer = '<p>2 is a prime number, because its only whole-number factors are 1 and itself.</p>'
    elif n % 2 == 0:
        answer = '<p>{n} is not a prime number, because it is divisible by 2.</p>'.format(n=n)

    sqr = int(sqrt(n)) + 1

    for divisor in range(3, sqr, 2):
        if n % divisor == 0:
            answer = '<p>{n} is not a prime number, because it is divisible by {divisor}'.format(n=n, divisor=divisor)

    if len(answer) == 0:
        answer = '<p>{n} is a prime number, because its only whole-number factors are 1 and itself</p>'.format(n=n)

    tips = '''
    <h6>Helpful Tips:</h6>
    <p>Prime numbers are numbers whose only whole-number factors are 1 and itself.</p>

    <p>To determine if a number is a prime number, here are some rules:</p>
    <ul>
        <li>Prime numbers are defined for whole-numbers greater than 1.</li>
        <li>1 is not considered a prime number.</li>
        <li>2 is considered a prime number because its only whole-number factors are 1 and itself.</li>
        <li>Any number that is divisible by 2 is not a prime number.</li>
        <li>After the steps above, start by dividing the number by 3, then 5, then 7, and so on, checking if the number
            can
            be divided by those divisors. As we have determined it cannot be divided by 2, there is no need to divide by
            4,
            6, 8, and so on.
        </li>
        <li>The maximum divisor to look for is the square root of your number, because n = a &times b and if both values
            were greater than the square root of n, a &times b would be larger than n. Therefore at least one of those
            factors must be less than or equal to the square root of n.
        </li>
    </ul>
    '''
    content = '{answer}{tips}'.format(answer=answer, tips=tips)
    return content


def find_prime_factors(n: int) -> tuple:
    """
    Find prime factors of a given number with proof and answer.

    :param n: a number
    :type n: int
    :return: returns factors (a list of prime factors), finding_prime_factors_proof (html of proof of how to find prime
    factors), and prime_factors_proof (html of proof that these are the prime factors)
    :rtype: tuple(list, str, str)
    """
    finding_prime_factors_proof = []
    original_n = int(n)

    ######################
    # find prime factors #
    ######################
    i = 2
    prime_factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            prime_factors.append(i)
            finding_prime_factors_proof.append('''
            <tr>
            <td>{n} | {factor}</td>
            <td class="column-wrap mdl-data-table__cell--non-numeric">{factor} is a factor of {n}</td>
            <td class="column-wrap mdl-data-table__cell--non-numeric">{n} divided by {factor} is {answer}</td>
            </tr>
            '''.format(n=n, factor=i, answer=n // i))
            n //= i

    if n > 1:
        prime_factors.append(n)
        finding_prime_factors_proof.append('''
        <tr>
        <td>{n} | {factor}</td>
        <td class="column-wrap mdl-data-table__cell--non-numeric">{factor} is a factor of {n}</td>
        <td class="column-wrap mdl-data-table__cell--non-numeric">{n} divided by {factor} is {answer}</td>
        </tr>
        '''.format(n=n, factor=n, answer='1'))
    finding_prime_factors_proof.append('<tr><td>1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td></td><td></td></tr>')

    finding_prime_factors_proof = ''.join([proof_step for proof_step in finding_prime_factors_proof])
    finding_prime_factors_proof = '''
    <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable">
    <tbody>
    {finding_prime_factors_proof}
    </tbody>
    </table>
    '''.format(finding_prime_factors_proof=finding_prime_factors_proof)

    #######################
    # prime_factors_proof #
    #######################
    prime_factors_proof = ''
    for prime_factor in prime_factors:
        prime_factors_proof = '{prime_factors_proof}{prime_factor} &times '.format(
            prime_factors_proof=prime_factors_proof, prime_factor=prime_factor)
    prime_factors_proof = '{prime_factors_proof} = {answer}<br>'.format(prime_factors_proof=prime_factors_proof[:-8],
                                                                        answer=original_n)

    for prime_factor in sorted(set(prime_factors)):
        prime_factors_proof = '{prime_factors_proof}{prime_factor}<sup>{exponent}</sup> &times '.format(
            prime_factors_proof=prime_factors_proof, prime_factor=prime_factor,
            exponent=prime_factors.count(prime_factor))
    prime_factors_proof = '{prime_factors_proof} = {answer}'.format(prime_factors_proof=prime_factors_proof[:-8],
                                                                    answer=original_n)
    return prime_factors, finding_prime_factors_proof, prime_factors_proof


def highest_common_factor(n_1: int, n_2: int) -> str:
    """
    Find the highest common factor of two numbers, showing proof and answer.

    :param n_1: first number
    :type n_1: int
    :param n_2: second number
    :type n_2: int
    :return: html containing proof and answer of highest common factor of two numbers
    :rtype: str
    """
    # get primes and output of num_1 and num_2
    prime_factors_1, finding_prime_factors_proof_1, prime_factors_proof_1 = find_prime_factors(n_1)
    prime_factors_2, finding_prime_factors_proof_2, prime_factors_proof_2 = find_prime_factors(n_2)

    # find shared primes elements in each list:
    shared_primes = []
    prime_factors_1_compare = list(prime_factors_1)
    for i in prime_factors_2:
        if i in prime_factors_1_compare:
            shared_primes.append(i)
            prime_factors_1_compare.remove(i)

    if len(shared_primes) == 0:
        shared_primes.append(1)

    # shared primes
    shared_primes_proof = ''
    for shared_prime in sorted(shared_primes):
        shared_primes_proof = '{shared_primes_proof}{shared_prime}, '.format(
            shared_primes_proof=shared_primes_proof,
            shared_prime=shared_prime)
    shared_primes_proof = shared_primes_proof[:-2]

    # calculate the highest common factor (the number)
    hcf_proof = ''
    hcf = 1
    for prime in sorted(shared_primes):
        hcf_proof = '{hcf_proof}{prime} &times '.format(hcf_proof=hcf_proof, prime=prime)
        hcf = prime * hcf
    hcf_proof = '{hcf_proof} = {hcf}'.format(hcf_proof=hcf_proof[:-8], hcf=hcf)

    content = '''
    <p>Finding all prime factors of {n_1}:</p>
    <table class="mdl-data-table mdl-js-data-table">
    {finding_prime_factors_proof_1}
    </table>
    <br>

    <p>Finding all prime factors of {n_2}:</p>
    <table class="mdl-data-table mdl-js-data-table">
    {finding_prime_factors_proof_2}
    </table>
    <br>

    <p>Prime factors for the first number are:<br>
    {prime_factors_proof_1}</p>

    <p>Prime factors for the second number are:<br>
    {prime_factors_proof_2}</p>

    <p>Find the primes that are shared between the two numbers:<br>
    {shared_primes_proof}</p>

    <p>Take the shared primes and multiply them together:<br>
    {hcf_proof}</p>

    <p>Therefore the highest common factor of {n_1} and {n_2} is:<br>
    {hcf}</p>
    '''.format(n_1=n_1, finding_prime_factors_proof_1=finding_prime_factors_proof_1,
               prime_factors_proof_1=prime_factors_proof_1,
               n_2=n_2, finding_prime_factors_proof_2=finding_prime_factors_proof_2,
               prime_factors_proof_2=prime_factors_proof_2,
               shared_primes_proof=shared_primes_proof, hcf_proof=hcf_proof, hcf=hcf)
    return content


def lowest_common_multiple(n_1: int, n_2: int) -> str:
    """
    Find the lowest common multiple of two numbers, showing proof and answer.

    :param n_1: first number
    :type n_1: int
    :param n_2: second number
    :type n_2: int
    :return: html containing proof and answer of lowest common multiple of two numbers
    :rtype: str
    """
    # get primes and output of num_1 and num_2
    prime_factors_1, finding_prime_factors_proof_1, prime_factors_proof_1 = find_prime_factors(n_1)
    prime_factors_2, finding_prime_factors_proof_2, prime_factors_proof_2 = find_prime_factors(n_2)

    # multiply prime sets
    prime_factor_set = {}
    answer = 1
    for n in set(prime_factors_1):
        if prime_factors_2.count(n):
            if prime_factors_1.count(n) >= prime_factors_2.count(n):
                prime_factor_set[n] = '{n}<sup>{exponent}</sup> &times '.format(n=n, exponent=prime_factors_1.count(n))
                answer *= pow(n, prime_factors_1.count(n))
        else:
            prime_factor_set[n] = '{n}<sup>{exponent}</sup> &times '.format(n=n, exponent=prime_factors_1.count(n))
            answer *= pow(n, prime_factors_1.count(n))

    for n in set(prime_factors_2):
        if prime_factors_1.count(n):
            if prime_factors_2.count(n) > prime_factors_1.count(n):
                prime_factor_set[n] = '{n}<sup>{exponent}</sup> &times '.format(n=n, exponent=prime_factors_2.count(n))
                answer *= pow(n, prime_factors_2.count(n))
        else:
            prime_factor_set[n] = '{n}<sup>{exponent}</sup> &times '.format(n=n, exponent=prime_factors_2.count(n))
            answer *= pow(n, prime_factors_2.count(n))

    # join prime_sets into a single string
    lcm_proof = ''
    for i in sorted(prime_factor_set):
        lcm_proof = '{lcm_proof}{prime_factor_set}'.format(lcm_proof=lcm_proof, prime_factor_set=prime_factor_set[i])
    lcm_proof = '{lcm_proof} = {answer}'.format(lcm_proof=lcm_proof[:-8], answer=answer)

    content = '''
    <p>Finding all prime factors of {n_1}:</p>
    <table class="mdl-data-table mdl-js-data-table">
    {finding_prime_factors_proof_1}
    </table>
    <br>

    <p>Finding all prime factors of {n_2}:</p>
    <table class="mdl-data-table mdl-js-data-table">
    {finding_prime_factors_proof_2}
    </table>
    <br>

    <p>Prime factors for the first number are:<br>
    {prime_factors_proof_1}</p>

    <p>Prime factors for the second number are:<br>
    {prime_factors_proof_2}</p>

    <p>Compare the primes of the first and second numbers and use the sets with the highest exponent:<br>
    {lcm_proof}</p>

    <p>Therefore the lowest common multiple of {n_1} and {n_2} is:<br>
    {answer}</p>
    '''.format(n_1=n_1, finding_prime_factors_proof_1=finding_prime_factors_proof_1, n_2=n_2,
               finding_prime_factors_proof_2=finding_prime_factors_proof_2, prime_factors_proof_1=prime_factors_proof_1,
               prime_factors_proof_2=prime_factors_proof_2, lcm_proof=lcm_proof, answer=answer)
    return content
