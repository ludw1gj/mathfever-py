from collections import OrderedDict
from decimal import Decimal, ROUND_HALF_UP


def binary_validation(binaries: OrderedDict) -> bool:
    """
    Check if is a valid binary value, either 0's and/or 1's.

    :param binaries: OrderedDict containing binary str values that must not contain spaces.
    :type binaries: OrderedDict
    :return: True if is binary or False if not binary.
    :rtype: bool
    """
    binary_digits = ('0', '1')
    is_binary = True

    for binary in binaries.values():
        if len(binary) > 64:
            is_binary = False
            break
        for digit in binary:
            if digit not in binary_digits:
                is_binary = False
                break

    return is_binary


def decimal_validation(decimals: OrderedDict) -> bool:
    """
    Check if is a valid decimal value, 0-9.

    :param decimals: OrderedDict containing decimal str values that must not contain spaces.
    :type decimals: OrderedDict
    :return: True if is decimal or False if not decimal.
    :rtype: bool
    """
    is_decimal = True

    for decimal in decimals.values():
        if int(decimal) > 999999999999:
            is_decimal = False
            break
        if not str.isdecimal(decimal):
            is_decimal = False
            break

    return is_decimal


def hexadecimal_validation(hexadecimals: OrderedDict) -> bool:
    """
    Check if is a valid hexadecimal value, 0-9 A-F.

    :param hexadecimals: OrderedDict containing hexadecimal str values that must not contain spaces and must be in
    uppercase
    :type hexadecimals: OrderedDict
    :return: True if is hexadecimal or False if not hexadecimal.
    :rtype: bool
    """
    hexadecimal_chars = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
    is_hexadecimal = True

    for hexadecimal in hexadecimals.values():
        if len(hexadecimal) > 64:
            is_hexadecimal = False
            break
        for char in hexadecimal:
            if char not in hexadecimal_chars:
                is_hexadecimal = False
                break

    return is_hexadecimal


def normalize_quantize(d: Decimal) -> Decimal:
    """
    Quantize a decimal.Decimal. If it's a whole number: remove unnecessary 0's beyond decimal point, or if it's not:
    round half up to 2 decimal points.

    :param d: A number of type decimal.Decimal
    :type d: Decimal
    :return: normalized Decimal
    :rtype: Decimal
    """
    if d == d.to_integral():
        return d.quantize(Decimal('1'), rounding=ROUND_HALF_UP)
    else:
        return d.quantize(Decimal('.01'), rounding=ROUND_HALF_UP).normalize()
