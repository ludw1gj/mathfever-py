from django.test import TestCase
from calculation.models import Category, Calculation, Post
from django.utils import timezone


# Model Tests
class CategoryTestCase(TestCase):
    def setUp(self):
        Category.objects.create(title="Test Category Title",
                                body="This is a test category body.",
                                image="/dummy/url.jpg",
                                meta='Test category meta.')

    def test_category_creation(self):
        """Test the creation of Categories"""
        test_category = Category.objects.get(title="Test Category Title")
        self.assertEqual(test_category.__str__(), test_category.title)


class CalculationTestCase(TestCase):
    def setUp(self):
        Category.objects.create(title="Test Category Title",
                                body="This is a test category body.",
                                image="/dummy/url.jpg",
                                meta='Test category meta.')

        Calculation.objects.create(category=Category.objects.get(title="Test Category Title"),
                                   title="Test Calculation Title",
                                   input_labels="('Test Input Label 1', 'Test Input Label 2')",
                                   input_ids="('testInputLabel1', 'testInputLabel2')",
                                   input_type='decimal_int',
                                   function='test_function()',
                                   meta='Test calculation meta.')

    def test_calculation_creation(self):
        """Test the creation of Calculations"""
        test_calculation = Calculation.objects.get(title="Test Calculation Title")
        # note: input_labels and input_ids are indeed a list when submitted as a string.
        self.assertEqual(test_calculation.__str__(), test_calculation.title)


class PostTestCase(TestCase):
    def setUp(self):
        Post.objects.create(title="Test Post Title",
                            body="This is a test post body.",
                            image="/dummy/url.jpg",
                            status='d',
                            created=timezone.now(),
                            modified=timezone.now())

    def test_post_creation(self):
        """Test the creation of Posts"""
        test_post = Post.objects.get(title="Test Post Title")
        self.assertEqual(test_post.__str__(), test_post.title)
