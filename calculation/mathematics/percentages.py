from decimal import Decimal
from .validation import normalize_quantize


def increase_decrease_by_percentage(p: Decimal, n: Decimal) -> str:
    """
    Increase/Decrease a number by a percentage.
    (n / 100) * p + n

    First work out 1% of 250, 250 ÷ 100 = 2.5 then multiply the answer by 23, because there was a 23% increase in
    rainfall. 2.5 × 23 = 57.5.

    :param p: percentage
    :type p: decimal.Decimal
    :param n: number
    :type n: decimal.Decimal
    :return: HTML containing proof and answer
    :rtype: str
    """
    one_percent = normalize_quantize(Decimal(n / 100))
    add_value = normalize_quantize(Decimal(one_percent * p))
    answer = normalize_quantize(Decimal(add_value + n))

    if p < 0:
        noun = 'decrease'
    else:
        noun = 'increase'

    content = '''
    <p>Where the {noun} in Percentage is {p}%, and the Number is {n}:</p>

    <p>Calculate using the equation shown below:<br>
    (Number &#xf7; 100) &times Percentage + Number</p>

    <p>Find 1% of {n}:<br>
    Number &#xf7; 100<br>
    {n} &#xf7; 100<br>
    {one_percent}</p>

    <p>Multiply the one percent value by {p} to find {p}% of {n}:<br>
    {one_percent} &times Percentage<br>
    {one_percent} &times {p}<br>
    {add_value}</p>

    <p>The value {add_value} is {p}% of {n}, add it to the Number to get the final answer:<br>
    {add_value} + Number<br>
    {add_value} + {n}<br>
    {answer}</p>

    <p>Therefore:<br>
    {n} {noun}d by {p}% is {answer}.</p>
    '''.format(noun=noun, p=p, n=n, one_percent=one_percent, add_value=add_value, answer=answer)
    return content


def number_from_percentage(p: Decimal, n: Decimal) -> str:
    """
    Find the value of a percentage of a number.
    (p / 100) * n

    :param p: percentage
    :type p: decimal.Decimal
    :param n: a number
    :type n: decimal.Decimal
    :return: HTML containing proof and answer
    :rtype: str
    """
    divided = normalize_quantize(Decimal(p / 100))
    answer = normalize_quantize(Decimal(divided * n))

    content = '''
    <p>Where the Percentage is {p}%, and the Number is {n}:</p>

    <p>Use the equation below:<br>
    (Percentage &#xf7; 100) &times Number<br>
    ({p} &#xf7; 100) &times {n}</p>

    <p>Find the decimalised version of the Percentage:<br>
    Percentage &#xf7; 100<br>
    {p} &#xf7; 100<br>
    {divided}</p>

    <p>Now multiply it with the Number to get the final answer:<br>
    ({divided}) &times Number<br>
    ({divided}) &times {n}<br>
    {answer}</p>

    <p>Therefore:<br>
    {p}% of {n} is {answer}.</p>
    '''.format(p=p, n=n, divided=divided, answer=answer)
    return content


def percentage_change(original_n: Decimal, new_n: Decimal) -> str:
    """
    Find the percentage change from one number to another.
    (new_n - original_n) / original_n * 100

    :param original_n: original number
    :type original_n: decimal.Decimal
    :param new_n: the new number
    :type new_n: decimal.Decimal
    :return: HTML containing proof and answer
    :rtype: str
    """
    change = normalize_quantize(Decimal(new_n - original_n))
    decimalised_p = normalize_quantize(Decimal(change / original_n))
    answer = normalize_quantize(Decimal(decimalised_p * 100))

    content = '''
    <p>Where Original Number is {original_n}, and the New Number is {new_n}:</p>

    <p>Calculate using the equation shown below:<br>
    ((New Number - Original Number) &#xf7; Original Number) &times 100<br>

    <p>Find the change in value between the two numbers:<br>
    New Number - Original Number<br>
    {new_n} - {original_n}<br>
    {change}</p>

    <p>Find the decimalised version of the percentage change using the change in value:<br>
    {change} &#xf7; Original Number<br>
    {change} &#xf7; {original_n}<br>
    {decimalised_p}</p>

    <p>Convert decimalised percentage to percentage to get the final answer:<br>
    {decimalised_p} &times 100<br>
    {answer}</p>

    <p>Therefore:<br>
    The percentage of change from {original_n} to {new_n} is {answer}%.</p>
    '''.format(original_n=original_n, new_n=new_n, change=change, decimalised_p=decimalised_p, answer=answer)
    return content


def percentage_from_number(n: Decimal, total_n: Decimal) -> str:
    """
    Find the percentage of a number of a total number.
    (n / total_n) * 100

    :param n: a number
    :type n: Decimal decimal.Decimal
    :param total_n:
    :type total_n: decimal.Decimal
    :return: HTML containing proof and answer
    :rtype: str
    """
    decimalised_p = normalize_quantize(Decimal(n / total_n))
    answer = normalize_quantize(Decimal(decimalised_p * 100))

    content = '''
    <p>Where the Number is {n}, and the Total Number is {total_n}:</p>

    <p>Calculate using the equation shown below:<br>
    (Number &#xf7; Total Number) &times 100</p>

    <p>Find the decimalised version of the percentage:<br>
    Number &#xf7; Total Number<br>
    {n} &#xf7; {total_n}<br>
    {decimalised_p}</p>

    <p>Convert decimalised percentage to percentage to get the answer:<br>
    {decimalised_p} &times 100<br>
    {answer}</p>


    <p>Therefore:<br>
    {n} is {answer}% of {total_n}.</p>
        '''.format(n=n, total_n=total_n, decimalised_p=decimalised_p, answer=answer)
    return content
