from django.core.urlresolvers import reverse
from django.db import models
from django.template.defaultfilters import slugify
from django.utils import timezone
from django_mysql.models import ListTextField

STATUS_CHOICES = (
    ('d', 'Draft'),
    ('p', 'Published'),
)

INPUT_TYPE_CHOICES = (
    ('binary_str', 'Binary String'),
    ('integer_str', 'Integer String'),
    ('decimal_Decimal', 'Decimal decimal.Decimal'),
    ('decimal_int', 'Decimal Integer'),
    ('hexadecimal_str', 'Hexadecimal String'),
)


class Category(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True, editable=False)
    image = models.ImageField(upload_to="images/category-cards")
    body = models.TextField(blank=True)
    meta = models.TextField(blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('calculation:category', args=[self.slug])


class Calculation(models.Model):
    category = models.ForeignKey('Category', related_name='calculations')
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True, editable=False)
    input_labels = ListTextField(base_field=models.CharField(max_length=500))
    input_ids = ListTextField(base_field=models.CharField(max_length=500))
    input_type = models.CharField(max_length=100, choices=INPUT_TYPE_CHOICES)
    function = models.CharField(max_length=100)
    meta = models.TextField(blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Calculation, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('calculation:calculation', args=[self.category.slug, self.slug])

    def get_api_networking_absolute_url(self):
        return reverse('calculation:networking', args=[self.slug])


class Post(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True, editable=False)
    body = models.TextField()
    image = models.ImageField(upload_to="images/posts")
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        self.slug = slugify(self.title)
        return super(Post, self).save(*args, **kwargs)
