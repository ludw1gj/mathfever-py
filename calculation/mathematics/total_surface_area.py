from decimal import Decimal
from math import sqrt, pi
from .validation import normalize_quantize

# pi of type Decimal not Decimal
PI = Decimal(pi)


def pythagorean_theorem(side_a: Decimal, side_b: Decimal) -> str:
    """
    Use the Pythagorean Theorem to find side lengths (a or b), or the hypotenuse (c) of a right-angle triangle.

    Pythagorean Theorem: right-angle triangle with side lengths (a, b) and hypotenuse (c).
    a^2 + b^2 = c^2

    Example:
    When a = 25 and b = 17, find c.
    c^2 = 25^2 + 17^2
        = 625 + 289
        = 914
    c   = √914
        ≈ 30.2

    :param side_a: side length of right-angled triangle.
    :type side_a: Decimal
    :param side_b: the other side length of right-angled triangle.
    :type side_b: Decimal
    :return: HTML of equation and mathematical proof of answer.
    :rtype: str
    """
    a_squared = normalize_quantize(Decimal(pow(side_a, 2)))
    b_squared = normalize_quantize(Decimal(pow(side_b, 2)))
    a_squared_plus_b_squared = a_squared + b_squared

    answer = normalize_quantize(Decimal(sqrt(a_squared_plus_b_squared)))

    content = '''
    <p>The Pythagorean theorem, also known as Pythagoras's theorem, can be used when two sides of a right angled
    triangle are known, and then we can find the third by using the equation:<br>
    a<sup>2</sup> + b<sup>2</sup> = c<sup>2</sup></p>

    <p>Where a = {a} and b = {b}, find c (hypotenuse):<br>
    a<sup>2</sup> + b<sup>2</sup> = c<sup>2</sup></p>

    <p>Square a and b:<br>
    {a_squared} + {b_squared} = c<sup>2</sup></p>

    <p>Add a and b together:<br>
    {a_squared_plus_b_squared} = c<sup>2</sup></p>

    <p>Square root both sides:<br>
    √{a_squared_plus_b_squared} = √c<sup>2</sup><br>
    c = {answer}</p>

    <p>Therefore:<br>
    c is equal to {answer}</p>
    '''.format(a=side_a, b=side_b, a_squared=a_squared, b_squared=b_squared,
               a_squared_plus_b_squared=a_squared_plus_b_squared, answer=answer)
    return content


def tsa_cone(radius: Decimal, slant_height: Decimal) -> str:
    """
    Find the Total Surface Area of a sphere, with the equation:
    Where S is Slant Height, r is radius
    TSA = area of base (circle) + area of curved surface
        = πr^2 + πrS
        = πr(r + S)

    :param radius: radius of cone.
    :type radius: Decimal
    :param slant_height: height of the slant.
    :type slant_height: Decimal
    :return: HTML of equation and mathematical proof of answer.
    :rtype: str
    """
    area_of_base = normalize_quantize(Decimal(PI * pow(radius, 2)))
    area_of_curved_surface = normalize_quantize(Decimal(PI * radius * slant_height))

    tsa = normalize_quantize(Decimal(area_of_base + area_of_curved_surface))

    content = '''
    <p>Find the Total Surface Area of a cone, with the equation:<br>
    TSA = area of the base (circle) + area of the curved surface<br>
    TSA = π r<sup>2</sup> + π rS<br>

    <p>r is the radius and S is the Slant Height.</p>

    <p>Where r = {radius} and S = {slant_height}:<br>
    Find the area of the base:<br>
    π r<sup>2</sup><br>
    π &times {radius}<sup>2</sup><br>
    {area_of_base}</p>

    <p>Find the area of the curved surface:<br>
    π rS<br>
    π &times {radius} &times {slant_height}<br>
    {area_of_curved_surface}</p>

    <p>Add the two surface areas together:<br>
    {area_of_base} + {area_of_curved_surface}<br>
    {tsa}</p>

    <p>Therefore:<br>
    The total surface area of a cone with the radius of {radius} and a slant height of {slant_height}
    equals {tsa}.</p>
    '''.format(radius=radius, slant_height=slant_height, area_of_base=area_of_base,
               area_of_curved_surface=area_of_curved_surface, tsa=tsa)
    return content


def tsa_cube(length: Decimal) -> str:
    """
    Find the Total Surface Area of a cube, with the equation:
    TSA = 6L^2

    :param length: length of any one side of cube.
    :type length: Decimal
    :return: HTML of equation and mathematical proof of answer.
    :rtype: str
    """
    length_squared = normalize_quantize(Decimal(pow(length, 2)))
    tsa = normalize_quantize(Decimal(6 * length_squared))

    content = '''
    <p>Find the Total Surface Area of a cube, with the equation:<br>
    TSA = 6L<sup>2</sup></p>

    <p>L is the length of each side of the cube. The surface area of a single side is a square, thus L &times L, which
    is the same as L<sup>2</sup>. Since all of the sides are the same, and there are 6 sides, you times L<sup>2</sup> by
    6.</p>

    <p>Where L = {length}:<br>
    TSA = 6 &times {length}<sup>2</sup><br>
    TSA = 6 &times {length_squared}<br>
    TSA = {tsa}</p>

    <p>Therefore:<br>
    The total surface area of a cube with the length of {length} equals {tsa}.</p>
    '''.format(length=length, length_squared=length_squared, tsa=tsa)
    return content


def tsa_cylinder(radius: Decimal, height: Decimal) -> str:
    """
    Find the Total Surface Area of a cylinder, with the equation:
    TSA = area of 2 circles + curved surface
        = 2πr^2 + 2πrh
        = 2πr(r + h)

    :param radius: radius of cylinder.
    :type radius: Decimal
    :param height: height of cylinder.
    :type height: Decimal
    :return: HTML of equation and mathematical proof of answer.
    :rtype: str
    """
    radius_squared = normalize_quantize(Decimal(pow(radius, 2)))
    area_of_circles = normalize_quantize(Decimal(2 * PI * radius_squared))

    radius_multiplied_height = radius * height
    area_of_curved_surface = normalize_quantize(Decimal(2 * PI * radius_multiplied_height))

    tsa = normalize_quantize(Decimal(area_of_circles + area_of_curved_surface))

    content = '''
    <p>Find the Total Surface Area of a cylinder, with the equation:<br>
    TSA = area of 2 circles + area of curved surface<br>
    TSA = 2 π r<sup>2</sup> + 2 π rh</p>

    <p>r is the radius and h is the height. A cylinder consists of two circles and a curved surface. Find the area of
    one of the circles and multiply that by 2, find the the area of the curved surface, and then add those two together.
    </p>

    <p>Where r = {radius}, and h = {height}:<br>
    Find the area of the two circles:<br>
    2 π r<sup>2</sup><br>
    2 &times π &times {radius}<sup>2</sup><br>
    2 &times π &times {radius_squared}<br>
    {area_of_circles}</p>

    <p>Find the area of curved surface:<br>
    2 π rh<br>
    2 &times π &times {radius} &times {height}<br>
    2 &times π &times {radius_multiplied_height}<br>
    {area_of_curved_surface}</p>

    <p>Add the two surface areas together:<br>
    {area_of_circles} + {area_of_curved_surface}<br>
    {tsa}

    <p>Therefore:<br>
    The total surface area of a cylinder with the radius of {radius}, and a height of {height} equals {tsa}.</p>
    '''.format(radius=radius, height=height, radius_squared=radius_squared, area_of_circles=area_of_circles,
               radius_multiplied_height=radius_multiplied_height, area_of_curved_surface=area_of_curved_surface,
               tsa=tsa)
    return content


def tsa_rectangular_prism(height: Decimal, length: Decimal, width: Decimal) -> str:
    """
    Find the Total Surface Area of a rectangular prism, with the equation:
    TSA = 2(wh + lw + lh)

    :param height: height of rectangular prism.
    :type height: Decimal
    :param length: length of rectangular prism.
    :type length: Decimal
    :param width: width of rectangular prism.
    :type width: Decimal
    :return: HTML of equation and mathematical proof of answer.
    :rtype: str
    """
    addition = normalize_quantize(Decimal((width * height) + (length * width) + (length * height)))
    tsa = normalize_quantize(Decimal(2 * addition))

    content = '''
     <p>Find the Total Surface Area of a rectangular prism, with the equation:<br>
     TSA = 2(WH + LW + LH)</p>

    <p>A rectangular prism has a surface area of six rectangles. Two sides of each: width &times height, length &times
    width, and length &times height. That would be 2WH + 2LW + 2LH, which would be the same as 2(WH + LW + LH).</p>

    <p>Where H = 2, L = 4, W = 3:<br>
    TSA = 2({width} × {height} + {length} × {width} + {length} × {height})<br>
    TSA = 2({addition})<br>
    TSA = {tsa}</p>

    <p>Therefore:<br>
    The total surface area of a rectangular prism with the height of {height}, a length of {length}, and a width of
    {width} is equal to {tsa}.</p>
    '''.format(width=width, height=height, length=length, addition=addition, tsa=tsa)
    return content


def tsa_sphere(radius: Decimal) -> str:
    """
    Find the Total Surface Area of a sphere, with the equation:
    TSA = 4πr^2

    :param radius: radius of sphere.
    :type radius: Decimal
    :return: HTML of filled out equation and answer of the total surface area of sphere.
    :rtype: str
    """
    tsa = normalize_quantize(Decimal(4 * PI * pow(radius, 2)))

    content = '''
    <p>Find the Total Surface Area of a sphere, with the equation:<br>
    TSA = 4 π r<sup>2</sup></p>

    <p>r is the radius of the sphere.</p>

    <p>Where r = {radius}:<br>
    TSA = 4 &times π &times {radius}<sup>2</sup><br>
    TSA = {tsa}</p>

    <p>Therefore:<br>
    The total surface area of a sphere with the radius of {radius} equals {tsa}.</p>
    '''.format(radius=radius, tsa=tsa)
    return content


def tsa_square_based_triangle(base_length: Decimal, height: Decimal) -> str:
    """
    Find the Total Surface Area of a square based triangle, with the equation:
    TSA = area of square + area of 4 triangles
        = b^2 + 4 * (1/2bh)
        = b^2 + 2bh

    :param base_length: length of base.
    :type base_length: Decimal
    :param height: height of square based triangle.
    :type height: Decimal
    :return: HTML of equation and mathematical proof of answer.
    :rtype: str
    """
    area_of_square = normalize_quantize(Decimal(pow(base_length, 2)))
    area_of_4_triangles = normalize_quantize(Decimal(2 * (base_length * height)))

    tsa = normalize_quantize(Decimal(area_of_square + area_of_4_triangles))

    content = '''
    <p>Find the Total Surface Area of a square based triangle, with the equation:<br>
    TSA = area of the square + area of 4 triangles<br>
    TSA = b<sup>2</sup> + 4 &times (1/2bh)<br>
    TSA = b<sup>2</sup> + 2bh</p>

    <p>b is the base length, and h is the height.</p>

    <p>Where b = {base_length}, and h = {height}:<br>
    Area of the square:<br>
    b<sup>2</sup><br>
    {base_length}<sup>2</sup><br>
    {area_of_square}</p>

    <p>Area of 4 triangles:<br>
    2bh<br>
    2 &times {base_length} &times {height}<br>
    {area_of_4_triangles}</p>

    <p>Add the two surface areas together:<br>
    {area_of_square} + {area_of_4_triangles}<br>
    {tsa}</p>

    <p>Therefore:<br>
    The total surface area of a square based triangle with the base length of {base_length}, and a height is {height}
    equals {tsa}.</p>
    '''.format(base_length=base_length, height=height, area_of_square=area_of_square,
               area_of_4_triangles=area_of_4_triangles, tsa=tsa)
    return content
