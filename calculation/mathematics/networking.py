from math import floor


def binary_to_decimal(binary: str) -> str:
    """
    Convert a binary value to a decimal value, detailing each step of how a person would go about doing the calculation.

    :param binary: a binary value. Must not contain spaces.
    :type binary: str
    :return: HTML illustrating how to convert the inputting binary value to a decimal value, detailing the steps
    required to do the calculation, including the final answer.
    :rtype: str
    """
    base = 2
    n_position = len(binary) - 1
    proof_step_1, proof_step_2, proof_step_3, proof_step_4 = '', '', '', ''
    answer = 0

    # place each digit of the binary value into the equation (b x 2^n) with the corresponding nth position and simplify
    # it before going to the next digit
    for digit in binary:
        power = pow(base, n_position)
        step_value = int(digit) * power
        answer += step_value

        proof_step_1 = '{proof_step_1}(b={digit} &times; {base}<sup>n={n_position}</sup>) + '.format(
            proof_step_1=proof_step_1, digit=digit, base=base, n_position=n_position)
        proof_step_2 = '{proof_step_2}({digit} &times; {base}<sup>{n_position}</sup>) + '.format(
            proof_step_2=proof_step_2, digit=digit, base=base, n_position=n_position)
        proof_step_3 = '{proof_step_3}({digit} &times; {power}) + '.format(
            proof_step_3=proof_step_3, digit=digit, power=power)
        proof_step_4 = '{proof_step_4}({step_value}) + '.format(proof_step_4=proof_step_4, step_value=step_value)

        n_position -= 1

    proof = [proof_step_1, proof_step_2, proof_step_3, proof_step_4]

    # removes " + " from end of string values and adds a line break
    for i in range(len(proof)):
        proof[i] = '{binary} = {proof}<br>'.format(binary=binary, proof=proof[i][:-3])

    # make proof into a single string value
    proof = ''.join([proof_step for proof_step in proof])

    content = '''
    <p><span class="word-break">{binary}</span> is a binary number.</p>

    <p>Using the following continuing equation the accurate answer can be found: (b x 2<sup>n</sup>)…<br>
    Where <strong>b</strong> is the binary number at the n<sup>th</sup> position and
    <strong>n</strong> is the n<sup>th</sup> position.</p>

    <p>There are {length} numbers in this binary number, the n<sup>th</sup> position starts from 0, and therefore the
    n<sup>th</sup> position goes up to {n} ({length} - 1). Count down from {n} and insert the binary number at that
    position.</p>

    <p>The binary number <span class="word-break">{binary}</span> would be:<br>
    {proof}
    <span class="word-break">{binary}</span> = <span class="word-break">{answer}</span></p>

    <p class="word-break">Therefore:<br>
    ({binary})<sub>2</sub> = ({answer})<sub>10</sub></p>
    '''.format(n=len(binary) - 1, length=len(binary), binary=binary, proof=proof, answer=answer)
    return content


def binary_to_hexadecimal(binary: str) -> str:
    """
    Convert a binary value to a hexadecimal value, detailing each step of how a person would go about doing the
    calculation.

    :param binary: string containing a binary value. Must not contain spaces.
    :type binary: str
    :return: HTML illustrating how to convert the inputting binary value to a hexadecimal value, detailing the steps
    required to do the calculation, including the final answer.
    :rtype: str
    """
    binary_groups = ('0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111', '1000', '1001', '1010', '1011',
                     '1100', '1101', '1110', '1111')
    hexadecimal_chars = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
    original_binary = binary

    # if input doesn't divide into groups of length of 4, add 0's to the beginning of the string
    if len(binary) % 4 != 0:
        num = 4 - (len(binary) % 4)
        for i in range(num, 0, -1):
            binary = '0{binary}'.format(binary=binary)

    # split input binary into octets (groups of length of 4 digits)
    inputted_binary_groups = []
    for i in range(0, len(binary), 4):
        inputted_binary_group = binary[i:i + 4]
        inputted_binary_groups.append(inputted_binary_group)

    # compares the inputted binary octets to binary octets and if matched the equivalent hexadecimal character is
    # found. Then a string is produced of inputted binary octets, the equivalent decimal value and the equivalent
    # hexadecimal character. Also the answer is produced from the found hexadecimal equivalents.
    proof = []
    answer = []
    for inputted_binary_group in inputted_binary_groups:
        for index, binary_group in enumerate(binary_groups):
            if inputted_binary_group == binary_group:
                answer.append(hexadecimal_chars[index])
                decimal_answer = int(binary_group, 2)
                proof.append('({binary_group})<sub>2</sub> = ({decimal_answer})<sub>10</sub> = ({hexadecimal_char})'
                             '<sub>16</sub>'.format(binary_group=binary_group, decimal_answer=decimal_answer,
                                                    hexadecimal_char=hexadecimal_chars[index]))
                break

    groups = ' '.join([octet for octet in inputted_binary_groups])
    proof = '<br>'.join([proof_step for proof_step in proof])
    answer = ''.join(answer)

    content = '''
    <p><span class="word-break">{original_binary}</span> is a binary number.</p>

    <p>Break the binary number <span class="word-break">{original_binary}</span> into groups of 4 digits as such:<br>
    {groups}</p>

    <p>Convert those groups (base 2) to a decimal number (base 10), that way it is easy to convert them to a Hexadecimal
    number (base 16):<br>
    Refer to the Conversion Table <a href="/calculation/networking/" rel="noopener noreferrer" target="_blank">here</a>.
    <br>
    {proof}</p>

    <p class="word-break">Therefore:<br>
    ({original_binary})<sub>2</sub> = ({answer})<sub>16</sub></p>
    '''.format(original_binary=original_binary, binary=binary, groups=groups, proof=proof, answer=answer)
    return content


def decimal_to_binary(decimal: str) -> str:
    """
    Convert a decimal value to a binary value, detailing each step of how a person would go about doing the calculation.

    Divide the decimal number by 2. Treat the division as an integer division.
    Write down the remainder.
    Divide the result again by 2. Treat the division as an integer division.
    Repeat those steps until the result is 0.
    The binary value is the digit sequence of the remainders from the last to first.

    :param decimal: a decimal value. Must not contain spaces.
    :type decimal: str
    :return: HTML illustrating how to convert a decimal value to a binary value, detailing the steps required to do the
    calculation, including the answer.
    :rtype: str
    """
    binary_chars = ('0', '1')
    base = 2
    quotient = int(decimal)
    proof = []
    answer = []

    while quotient != 0:
        dividend = quotient
        quotient = floor(quotient / base)
        remainder = dividend % base

        proof_step = '{dividend} &divide; {base} = {quotient}, Remainder: {remainder}<br>'.format(
            dividend=dividend, base=base, quotient=quotient, remainder=remainder)
        proof.append(proof_step)

        for i in range(base):
            if remainder == i:
                answer.append(binary_chars[i])
                break

    proof = ''.join([proof_step for proof_step in proof])
    answer.reverse()
    answer = ''.join(answer)

    content = '''
    <p>{decimal} is a decimal number.</p>

    <p>Divide the decimal number by 2 and record the remainder:<br>
    {proof}</p>

    <p>Join the remainders from last to first:<br>
    <span class="word-break">{answer}</span></p>

    <p class="word-break">Therefore:<br>
    ({decimal})<sub>10</sub> = (<span class="word-break">{answer}</span>)<sub>2</sub></p>
    '''.format(decimal=decimal, proof=proof, answer=answer)
    return content


def decimal_to_hexadecimal(decimal: str) -> str:
    """
    Convert a decimal value to a hexadecimal value, detailing each step of how a person would go about doing the
    calculation.

    Divide the decimal number by 16. Treat the division as an integer division.
    Write down the remainder.
    Divide the result again by 16. Treat the division as an integer division.
    Repeat those steps until the result is 0.
    The hexadecimal value is the digit sequence of the remainders from the last to first.

    :param decimal: a decimal value. Must not contain spaces.
    :type decimal: str
    :return: HTML illustrating how to convert a decimal value to a hexadecimal value, detailing the steps required to
    do the calculation, including the answer.
    :rtype: str
    """
    hexadecimal_chars = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
    base = 16
    quotient = int(decimal)
    proof = []
    answer = []
    proof_remainder = []

    while quotient != 0:
        dividend = quotient
        quotient = floor(quotient / base)
        remainder = dividend % base

        proof_step = '{dividend} &divide; {base} = {quotient}, Remainder: {remainder}<br>'.format(
            dividend=dividend, base=base, quotient=quotient, remainder=remainder)
        proof.append(proof_step)

        for i in range(base):
            if remainder == i:
                answer.append(hexadecimal_chars[i])

                proof_remainder_step = '({remainder})<sub>10</sub> = ({hexadecimal_char})<sub>16</sub><br>'.format(
                    remainder=remainder, hexadecimal_char=hexadecimal_chars[i])
                proof_remainder.append(proof_remainder_step)
                break

    proof = ''.join([proof_step for proof_step in proof])
    proof_remainder = ''.join([proof_remainder_step for proof_remainder_step in proof_remainder])
    answer.reverse()
    answer = ''.join(answer)

    content = '''
    <p>{decimal} is a decimal number.</p>

    <p>Divide the decimal number by 16 and record the remainder:<br>
    {proof}</p>

    <p>Convert remainders to their hexadecimal equivalents:<br>
    Refer to the Conversion Table <a href="/calculation/networking/" rel="noopener noreferrer" target="_blank">here</a>.
    <br>
    {proof_remainder}</p>

    <p>Join the remainders from last to first:<br>
    {answer}</p>

    <p class="word-break">Therefore:<br>
    ({decimal})<sub>10</sub> = ({answer})<sub>2</sub></p>
    '''.format(decimal=decimal, proof=proof, proof_remainder=proof_remainder, answer=answer)
    return content


def hexadecimal_to_binary(hexadecimal: str) -> str:
    """
    Convert a hexadecimal value to a binary value, detailing each step of how a person would go about doing the
    calculation.

    :param hexadecimal: a hexadecimal value. Must not contain spaces.
    :type hexadecimal: str
    :return: HTML illustrating how to convert the inputting hexadecimal value to binary value, detailing the steps
    required to do the calculation, including the final answer.
    :rtype: str
    """
    hexadecimal_chars = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
    binary_octets = ('0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111', '1000', '1001', '1010', '1011',
                     '1100', '1101', '1110', '1111')

    proof = []
    answer = []
    # Add string to proof list showing the equivalent base 16 to the base 2 number, and adding the resulting base 2
    # octet into the answer list. If character in hexadecimal is A, the binary equivalent would be 1010.
    for char in hexadecimal:
        for index, hexadecimal_digit in enumerate(hexadecimal_chars):
            if char == hexadecimal_digit:
                proof.append('({char})<sub>16</sub> = ({binary_octet})<sub>2</sub>'.format(
                    char=char, binary_octet=binary_octets[index]))
                answer.append(binary_octets[index])
                break

    answer = ' '.join(answer)
    proof = '<br>'.join([proof_step for proof_step in proof])

    content = '''
    <p><span class="word-break">{hexadecimal}</span> is a hexadecimal number.</p>

    <p>Convert each hexadecimal digit manually to its binary equivalent:<br>
    Refer to the Conversion Table <a href="/calculation/networking/" rel="noopener noreferrer" target="_blank">here</a>.
    <br>
    {proof}</p>

    <p>Join the digit equivalents together from first to last:<br>
    {answer}</p>

    <p class="word-break">Therefore:<br>
    (<span class="word-break">{hexadecimal}</span>)<sub>16</sub> = ({answer})<sub>2</sub></p>
    '''.format(hexadecimal=hexadecimal, proof=proof, answer=answer)
    return content


def hexadecimal_to_decimal(hexadecimal: str) -> str:
    """
    Convert a hexadecimal value to a decimal value, detailing each step of how a person would go about doing the
    calculation.

    :param hexadecimal: a hexadecimal value. Must not contain spaces.
    :type hexadecimal: str
    :return: HTML illustrating how to convert the inputting hexadecimal value to decimal value, detailing the steps
    required to do the calculation, including the final answer.
    :rtype: str
    """
    hexadecimal_chars = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
    decimal_digits = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15')
    base = 16

    digit_position = len(hexadecimal) - 1
    decimal_equivalent_proof = ''
    digit_position_proof = '{hexadecimal}: '.format(hexadecimal=hexadecimal)
    decimal_equivalents = []

    # Get the decimal equivalents of the hexadecimal digits and get their digit position
    for char in hexadecimal:
        for index, hexadecimal_digit in enumerate(hexadecimal_chars):
            if char == hexadecimal_digit:
                decimal_equivalents.append(decimal_digits[index])

                decimal_equivalent_proof = '{decimal_equivalent_proof}({char})<sub>16</sub> = ({decimal_digit})' \
                                           '<sub>10</sub><br>'.format(decimal_equivalent_proof=decimal_equivalent_proof,
                                                                      char=char, decimal_digit=decimal_digits[index])

                digit_position_proof = '{digit_position_proof}{char} position is {digit_position}, '.format(
                    digit_position_proof=digit_position_proof, char=char, digit_position=digit_position)

                digit_position -= 1
                break

    # remove ', ' from the end and add '.' at the end
    digit_position_proof = '{digit_position_proof}.'.format(digit_position_proof=digit_position_proof[:-2])

    # reset hexadecimal_length for loop
    digit_position = len(hexadecimal) - 1

    main_proof_1 = ''
    main_proof_2 = ''
    main_proof_3 = ''
    answer = 0

    # loops through the decimal equivalent digits and generates the main proof and the answer
    for digit in decimal_equivalents:
        power = pow(base, digit_position)
        step_value = int(digit) * power
        answer += step_value

        main_proof_1 = '{main_proof_1}({digit} &times; {base}<sup>{hexadecimal_length}</sup>) + '.format(
            main_proof_1=main_proof_1, digit=digit, base=base, hexadecimal_length=digit_position)

        main_proof_2 = '{main_proof_2}({digit} &times; {power}) + '.format(
            main_proof_2=main_proof_2, digit=digit, power=power)

        main_proof_3 = '{main_proof_3}{step_value} + '.format(
            main_proof_3=main_proof_3, step_value=step_value)

        digit_position -= 1
    main_proof_list = [main_proof_1, main_proof_2, main_proof_3]

    # removes " + " from end of string values and makes contents from main_proof into a single string value
    main_proof = '<br><span class="word-break">{hexadecimal}</span> = '.format(hexadecimal=hexadecimal).join(
        [main_proof_step[:-3] for main_proof_step in main_proof_list])

    # adds '{hexadecimal} =' to the beginning of main_proof as previous loop doesn't add it, and adds the final answer
    main_proof = '<span class="word-break">{hexadecimal}</span> = {main_proof}<br>{hexadecimal} = {answer}'.format(
        hexadecimal=hexadecimal, main_proof=main_proof, answer=answer)

    content = '''
    <p class="word-break">{hexadecimal} is a hexadecimal number.</p>

    <p>Get the decimal equivalent of each hexadecimal digit:<br>
    Refer to the Conversion Table <a href="/calculation/networking/" rel="noopener noreferrer" target="_blank">here</a>.
    <br>
    {decimal_equivalent_proof}</p>

    <p>Multiply each hexadecimal digit with 16 power of digit position. Digit position starts from zero, right to
    left:<br>
    {digit_position_proof}<br>
    {main_proof}</p>

    <p class="word-break">Therefore:<br>
    ({hexadecimal})<sub>16</sub> = ({answer})<sub>10</sub></p>
    '''.format(hexadecimal=hexadecimal, decimal_equivalent_proof=decimal_equivalent_proof,
               digit_position_proof=digit_position_proof, main_proof=main_proof, answer=answer)

    return content
