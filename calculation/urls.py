from django.conf.urls import url
from . import views

app_name = 'calculation'

urlpatterns = [
    # GENERAL URLS
    url(r'^$', views.home_view, name='home'),
    url(r'^about/$', views.about_view, name='about'),
    url(r'^help/$', views.help_view, name='help'),
    url(r'^terms/$', views.terms_view, name='terms'),
    url(r'^privacy/$', views.privacy_view, name='privacy'),
    url(r'^message-board/$', views.message_board_view, name='message'),
    # CALCULATION URLS
    url(r'^calculation/(?P<category_slug>[-\w]+)/$', views.category_view, name='category'),
    url(r'^calculation/(?P<category_slug>[-\w]+)/(?P<calculation_slug>[-\w]+)/$', views.calculation_view,
        name='calculation'),
    # API URLS
    url(r'^api/posts/$', views.APIRoutePosts.as_view(), name='api_posts'),
    url(r'^api/calculation/(?P<category_slug>[-\w]+)/$', views.APIRouteCategory.as_view(), name='api_category'),
    url(r'^api/calculation/(?P<category_slug>[-\w]+)/(?P<calculation_slug>[-\w]+)/$',
        views.APIRouteCalculation.as_view(), name='api_calculation'),
]
