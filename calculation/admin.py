from django.contrib import admin

from .models import Category, Calculation, Post


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title']
    ordering = ['title']


class CalculationAdmin(admin.ModelAdmin):
    list_display = ['title', 'category']
    ordering = ['category', 'title']


class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'created', 'status']
    ordering = ['-created']
    actions = ['make_published', 'make_draft']

    def make_published(self, request, queryset):
        queryset.update(status='p')

    def make_draft(self, request, queryset):
        queryset.update(status='d')

    make_published.short_description = 'Mark selected posts as published'
    make_draft.short_description = 'Mark selected posts as draft'


admin.site.register(Category, CategoryAdmin)
admin.site.register(Calculation, CalculationAdmin)
admin.site.register(Post, PostAdmin)
