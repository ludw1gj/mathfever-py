from collections import OrderedDict
from decimal import Decimal

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from rest_framework.decorators import APIView
from rest_framework.views import Response

import calculation.mathematics.networking as networking
import calculation.mathematics.numbers as numbers
import calculation.mathematics.percentages as percentages
import calculation.mathematics.total_surface_area as tsa
from calculation.mathematics.validation import binary_validation, decimal_validation, hexadecimal_validation
from calculation.models import Category, Calculation, Post
from calculation.serializers import PostSerializer, CalculationSerializer, CategorySerializer


#################
# GENERAL VIEWS #
#################
def home_view(request):
    categories = Category.objects.all()
    meta = 'MathFever - A website where users can find mathematical proof and answers to common math problems, with ' \
           'values of their choosing.'
    return render(request, 'calculation/home.html', {'title': 'Home', 'categories': categories, 'meta': meta})


def about_view(request):
    return render(request, 'calculation/general/about.html', {'title': 'About'})


def help_view(request):
    return render(request, 'calculation/general/help.html', {'title': 'Help'})


def terms_view(request):
    return render(request, 'calculation/general/terms.html', {'title': 'Terms of Use'})


def privacy_view(request):
    return render(request, 'calculation/general/privacy.html', {'title': 'Privacy'})


def message_board_view(request):
    post_list = Post.objects.filter(status='p').order_by('-created')
    paginator = Paginator(post_list, 2)

    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        page = '1'
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, 'calculation/message.html', {'title': 'Message Board', 'posts': posts, 'page': page})


def category_view(request, category_slug):
    category = get_object_or_404(Category, slug=category_slug)
    meta = category.meta
    return render(request, 'calculation/general/category.html', {'title': category.title, 'category': category,
                                                                 'meta': meta})


def calculation_view(request, category_slug, calculation_slug):
    calculation = get_object_or_404(Calculation, category__slug=category_slug, slug=calculation_slug)
    category = calculation.category
    meta = calculation.meta

    input_ids = calculation.input_ids
    input_labels = calculation.input_labels
    form = zip(input_ids, input_labels)
    return render(request, 'calculation/calculation.html', {'title': calculation.title, 'form_name': calculation.slug,
                                                            'form': form, 'meta': meta, 'category': category})


#############
# API VIEWS #
#############
class APIRoutePosts(APIView):
    @staticmethod
    def get(request):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)


class APIRouteCategory(APIView):
    @staticmethod
    def get(request, category_slug):
        category = get_object_or_404(Category, slug=category_slug)
        serializer = CategorySerializer(category)
        return Response(serializer.data)


class APIRouteCalculation(APIView):
    @staticmethod
    def get(request, category_slug, calculation_slug):
        calculation = get_object_or_404(Calculation, category__slug=category_slug, slug=calculation_slug)
        serializer = CalculationSerializer(calculation)
        return Response(serializer.data)

    @staticmethod
    def post(request, category_slug, calculation_slug):
        calculation = get_object_or_404(Calculation, category__slug=category_slug, slug=calculation_slug)
        return APIHandler.process_api_request(request, eval(calculation.function), calculation.input_ids,
                                              calculation.input_type)


class APIHandler:
    """
    Called by APIRouteCalculation view via a post request. Its methods handle request.data and calling math function,
    returning a response of {'content': <str>}.
    """

    @staticmethod
    def process_api_request(request, math_function, accepted_arguments, input_type):
        """
        This method is used to process an api request. It handles the retrieval of data from request.data of param
        accepted_arguments. Then depending upon input_type it validates the data and responds with a json object.

        :param request: From rest_framework.request.Request
        :type request: rest_framework.request.Request
        :param math_function: The math function that needs to be used to the API route
        :type math_function: function
        :param accepted_arguments: The accepted arguments for the API route
        :type accepted_arguments: tuple(str)
        :param input_type: The type of input required for the function from param math_function
        :type input_type: str
        :return: Return a Response of json object with attribute of either 'content or 'error'
        :rtype: rest_framework.response.Response
        """
        request_data = OrderedDict()
        try:
            for argument in accepted_arguments:
                request_data[argument] = request.data[argument].replace(' ', '')
        except KeyError:
            return Response({'error': APIHandler.invalid_args(accepted_arguments)}, status=403)
        else:
            # binary input, and math function param requires str
            if input_type == 'binary_str':
                if binary_validation(request_data):
                    return Response({'content': math_function(*request_data.values())})
                else:
                    return Response({'error': APIHandler.invalid_input_length_limit('binary string')}, status=403)

            # hexadecimal input, and math function param requires str
            elif input_type == 'hexadecimal_str':
                if hexadecimal_validation(request_data):
                    return Response({'content': math_function(*request_data.values())})
                else:
                    return Response({'error': APIHandler.invalid_input_length_limit('hexadecimal')}, status=403)

            # whole number input, and math function param requires str
            elif input_type == 'integer_str':
                if decimal_validation(request_data):
                    try:
                        for argument in accepted_arguments:
                            request_data[argument] = str(int(request_data[argument]))
                    except ValueError:
                        return Response({'error': APIHandler.invalid_input_number_limit('whole number')}, status=403)
                    else:
                        return Response({'content': math_function(*request_data.values())})

                else:
                    return Response({'error': APIHandler.invalid_input_number_limit('whole number')}, status=403)

            # whole number input, and math function param requires int
            elif input_type == 'decimal_int':
                try:
                    for argument in accepted_arguments:
                        request_data[argument] = int(request_data[argument])
                except ValueError:
                    return Response({'error': APIHandler.invalid_input_number_limit('whole number')}, status=403)
                else:
                    return Response({'content': math_function(*request_data.values())})

            # number input, and math function param requires decimal.Decimal
            elif input_type == 'decimal_Decimal':
                try:
                    for argument in accepted_arguments:
                        request_data[argument] = tsa.normalize_quantize(Decimal(request_data[argument]))
                except ValueError:
                    return Response({'error': APIHandler.invalid_input_number_limit('decimal')}, status=403)
                else:
                    return Response({'content': math_function(*request_data.values())})
            else:
                raise Exception("Argument input_type is incorrect.")

    @staticmethod
    def invalid_args(args):
        return '<p>Invalid arguments. This API route accepts the following arguments: {args}.</p>'.format(args=args)

    @staticmethod
    def invalid_input_length_limit(valid_type):
        return '<p>User input is invalid, please enter only {valid_type} values. Must not be over 64 characters in ' \
               'length.</p>'.format(valid_type=valid_type)

    @staticmethod
    def invalid_input_number_limit(valid_type):
        return '<p>User input is invalid, please enter only {valid_type} values. Must not be over 999,999,999,999.' \
               '</p>'.format(valid_type=valid_type)


###############
# ERROR VIEWS #
###############
def page_not_found_view(request):
    return render(request, 'calculation/error/404.html', {'title': 'Page not found'}, status=404)


def server_error_view(request):
    return render(request, 'calculation/error/500.html', {'title': 'Server Error'}, status=500)
