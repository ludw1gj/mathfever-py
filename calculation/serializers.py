from rest_framework import serializers

from .models import Calculation, Category, Post


class CalculationSerializer(serializers.ModelSerializer):
    # display category title rather than id
    category = serializers.CharField(read_only=True, source='category.title')

    class Meta:
        model = Calculation
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'
