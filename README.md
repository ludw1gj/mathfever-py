# MathFever-py | ARCHIVED 2016

A website built on Django Framework, where users can find mathematical proof and answers to common
math problems, with values of their choosing. This project uses Django Framework, Material Design
Lite, and plain Javascript/CSS.

I rebuilt this project in golang [here](https://github.com/ludw1gj/mathfever-go).

## Project Dependencies

```
Django==1.10.4
django-mysql==1.1.0
djangorestframework==3.5.3
image==1.5.5
mysqlclient==1.3.9
Pillow==3.4.2
six==1.10.0
```

## Setup Notes:

- When cloning this repository, do not forget to include a settings_secret.py file in the same
  directory as settings.py, or add SECRET_KEY setting within settings.py and delete the
  secret_settings.py import.

Create a settings_secret.py file in the same directory as settings.py consisting of:<br>
`SECRET_KEY = <YOUR_OWN_SECURITY_KEY>`

## Setup (virtualenv)

### Ubuntu 16.04

- Install dependencies

```
$ apt install python3-dev libmysqlclient-dev build-essential virtualenv -y
```

- Create a virtualenv

```
$ virtualenv -p python3 ~/djangoenv/mathfever-py
```

- Clone MathFever repository

```
$ git clone https://github.com/ludw1gj/mathfever-py ~/djangoenv/mathfever-py/mathfever-py
```

- Install requirements.text

```
$ source ~/djangoenv/mathfever-py/bin/activate
$ pip install -r ~/djangoenv/mathfever-py/mathfever-py/requirements.txt
$ deactivate
```

- Run the development server

```
$ source ~/djangoenv/mathfever-py/bin/activate
$ python ~/djangoenv/mathfever-py/mathfever-py/manage.py runserver
```

> Don't forget to include a settings_secret.py file.

### macOS Sierra 10.12

- Install dependencies

```
$ brew install mysql
```

- Create a virtualenv

```
$ pip install virtualenv
$ virtualenv -p python3 ~/djangoenv/mathfever-py
```

- Clone MathFever repository

```
$ git clone https://github.com/ludw1gj/mathfever-py ~/djangoenv/mathfever-py/mathfever-py
```

- Install requirements.text

```
$ source ~/djangoenv/mathfever-py/bin/activate
$ pip install -r ~/djangoenv/mathfever-py/mathfever-py/requirements.txt
$ deactivate
```

- Run the development server

```
$ source ~/djangoenv/mathfever-py/bin/activate
$ python ~/djangoenv/mathfever-py/mathfever-py/manage.py runserver
```

> Don't forget to include a settings_secret.py file.
